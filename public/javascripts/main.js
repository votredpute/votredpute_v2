// Pour la zone de recherche dans le menu du haut
$(function () {

    $.ajax({url: "/deputes-json", success: function(results){
        results = JSON.parse(results);
        var html = "";
        $.each(results , function(k,v){
            html+= "<option value= \""+v.memberships[0].riding.id+"\">" +v["name"]+ "</option>"
        });
        $("#listDep").html(html);
    }});

    $('#rechercheForm').submit(function (evt) {
        evt.preventDefault();
        var name = $('#barre-nav-champ-recherche').val();
        window.location = `/rechercher?name=${name}`;


    });
    $('#rechercheForm2').submit(function (evt) {
        evt.preventDefault();
        var name = $('#barre-nav-champ-recherche2').val();
        window.location = `/rechercher?name=${name}`;
    });

    $('#rechercheForm3').submit(function (evt) {
        evt.preventDefault();
        var name = $('#barre-nav-champ-recherche3').val();
            });

    $("#listDep").on("change", function(){
        var value = this.value;
        var lien = "/depute-profil-votes-json/" + value;
        $("#hidden").attr("id", "visible");
        $('#table2').DataTable().destroy();
        $('#table2').DataTable( {

                "ajax": lien 
            } );
        
    });
});

