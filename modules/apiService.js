let http = require("http");
let mongodb = require('mongodb');

class APIService {
    getCollection(collName) {
        let collection;

        return  new Promise(function(resolve, reject){
            if (collection && collection == collName){
                resolve(collection)
            }else{
                getConnection()
                    .then((db)=> {
                        db.collection(collName, function (err, coll) {
                            if (err) {
                                reject(new Error(500));
                            } else {
                                collection = coll;
                                resolve(coll);
                            }
                        });
                    })
                    .catch((error)=>{
                        "use strict";
                        console.log(`Error ::  ${error}`);
                    });
            }

        });
    }
    getNameFromPostalCode(postalCode){
        return new Promise(function (resolve,reject) {
            if (!isPostCode(postalCode)) {
                resolve(postalCode);
            } else {
                postalCode = postalCode.toUpperCase().replace(/\s/g, '');
                let url = `http://represent.opennorth.ca/postcodes/${postalCode}/?sets=federal-electoral-districts&format=json`;
                let request = http.get(url, function ( res) {

                    let body = "";

                    res.setEncoding('binary');

                    res.on('data', function (chunk) {
                        body += chunk;
                    });

                    res.on('end', function () {
                        let name = JSON.parse(body)['representatives_centroid'][0].name;
                       resolve(name);
                    });
                });
                request.on('error', function() {
                    let errorMsg = 'Could not connection to -donnees.ville.montreal.qc.ca:80-';
                    reject(new Error(errorMsg,500))
                });

            }

        });
    }
}

function getConnection() {
    let database;
    return new Promise(function(resolve, reject){
        if (database) {
            resolve(database);

        } else {

            let url = 'mongodb://votredepute:inf6150@ds139899.mlab.com:39899/heroku_fpw34v7h';

            

            mongodb.MongoClient.connect(url, function (err, db) {
                if (err) {
                    console.log("Connecting probleme");
                    reject(new Error(500));

                } else {
                    database = db;
                    resolve(db);
                }
            });
        }
    });
}

function isPostCode(postalCode) {
    let regex = new RegExp(/^[A-Z]\d[A-Z]\s?\d[A-Z]\d$/i);
    return regex.test(postalCode);
}


module.exports = new APIService();