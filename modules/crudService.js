let APIService = require('./apiService');


class CRUDService {
    find(collName, query, pageSize, projection) {
        return new Promise(function(resolve) {
            APIService.getCollection(collName)
                .then((collection)=>{
                    return collection.find(query, projection).limit(pageSize) ;
                })
                .then((elements)=>{
                    resolve(elements.toArray())
                })
                .catch((error)=>{
                    console.log(`Erreur du service CRUDService.find : ${error}`);
                });
        });
    }

    findOne(collName, query){
        return new Promise(function(resolve) {
            APIService.getCollection(collName)
                .then((collection)=>{
                    return collection.findOne(query) ;
                })
                .then((element)=>{
                    resolve(element)
                })
                .catch((error)=>{
                    console.log(`Erreur du service CRUDService.findOne : ${error}`);
                });
        });
    }

    findDepute(collName, query){
        return new Promise(function(resolve) {
            APIService.getCollection(collName)
                .then((collection)=>{
                    return collection.find(query) ;
                })
                .then((elements)=>{
                    resolve(elements.toArray())
                })
                .catch((error)=>{
                    console.log(`Erreur du service CRUDService.findDepute : ${error}`);
                });
        });
    }

}

module.exports = new CRUDService();