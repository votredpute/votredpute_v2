let express = require('express');
let CRUDService = require("../modules/crudService.js");
let apiService = require("../modules/apiService.js");
let router = express.Router();
let raml2html = require('raml2html');

//==================================================================
//   the routes qui n'etulise pas de collection
//      - Page d'accueil   ==> /
//      - A propos de nous ==> /a-notre-sujet'
//      - Documentation    ==> /doc
//==================================================================

router.get('/', function(req, res, next) {
    res.render('pages/index');
});

router.get('/a-notre-sujet', function(req, res) {
    res.render('pages/a-notre-sujet');
});

router.get('/doc', function (req, res) {


    const slateConfig = raml2html.getConfigForTheme('raml2html-slate-theme');

// source can be a filename, url or parsed RAML object
    const source = '../documentation/api.raml';
    raml2html.render(source, slateConfig)
        .then((html) => res.send(html))
        .catch((error) => console.error(error));
    // raml2html.render('../documentation/api.raml', config)
    //     .then((html)=> {
    //         res.send(html);
    //     })
    //     .catch((error)=> {
    //         let errMsg = "" +
    //             "Erreur Interne du Serveur :: " +
    //             "Le serveur a rencontré une condition inattendue qui " +
    //             "l'a empêché de répondre à votre demande";
    //         res.status(500).render('error', {error: error , message: errMsg})
    //
    //     })
});

//==================================================================
//   the routes that use the "deputes" collection
//     - /deputes
//     - /statistiques
//     - /depute-profil-infos/:id
//     - /depute-profil-depenses/:id
//     - /rechercher
//==================================================================

router.get('/deputes', function(req, res) {
     // ({_id: {$gte: current_id}}).skip(N * page_size).limit(page_size).sort({_id: 1});

     let query = [{}];
     let pageSize = 12;
     CRUDService.find('deputes',query[0],pageSize)
         .then((elements)=> {
             "use strict";
             if (elements.length == 0){
                 let errMsg = "" +
                     "Le serveur a traité avec succès la requête" +
                     " mais aucun contenu n'a été trouvé " ;
                 res.status(200).render('error', {  message: errMsg})
             }
             res.status(200);
             res.render('pages/deputes', {politiciens: elements});
         })
         .catch((error)=> {
             "use strict";
             console.log(`From index.js /deputes ${error}`);
         });
 });

router.get('/depute-profil-comparaison/:id', function(req, res) {
    let deputeId = parseInt(req.params.id);
    let query = [ { "memberships.end_date": null, "memberships.riding.id": deputeId }];
    CRUDService.findOne('deputes',query[0])
        .then((element)=> {
            "use strict";
            if (!element){
                let errMsg = "" +
                    "Le serveur a traité avec succès la requête" +
                    " mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg})
            }
            console.log(element);
            res.status(200);
            res.render('pages/depute-profil-comparaison', {depute: element});
        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /depute-profil-comparaison/:id ${error}`);
        });

});

router.get('/depute-profil-infos/:id', function(req, res) {
    let deputeId = parseInt(req.params.id);
    let query = [ { "memberships.end_date": null, "memberships.riding.id": deputeId }];
    CRUDService.findOne('deputes',query[0])
        .then((element)=> {
            "use strict";
            if (!element){
                let errMsg = "" +
                    "Le serveur a traité avec succès la requête" +
                    " mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg})
            }
            console.log(element);
            res.status(200);
            res.render('pages/depute-profil-infos', {depute: element});
        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /depute-profil-infos/:id ${error}`);
        });

});

router.get('/depute-profil-depenses/:id', function(req, res) {
    let deputeId = parseInt(req.params.id);
    let query =[ { "memberships.riding.id": deputeId }];
    CRUDService.findOne('deputes',query[0])
        .then((element)=> {
            "use strict";
            if (!element){
                let errMsg = "" +
                    "Le serveur a traité avec succès la requête" +
                    " mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg})
            }
            res.status(200);
            res.render('pages/depute-profil-depenses', {depute: element});
        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /depute-profil-depenses/:id ${error}`);
        });

});
router.get('/loi/id/:id' , function(req,res) {
    let loiId = parseInt(req.params.id);
    let query =[ {"legisinfo_id": loiId}];

    CRUDService.findOne('projetsloi',query[0])
        .then((loiTrouve)=> {
            "use strict";
            if (!loiTrouve){
                let errMsg = "" +
                    "Le serveur a traité avec succès la requête" +
                    " mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg})
            }
            res.status(200);
            res.render('pages/loi', {loi: loiTrouve});
        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /loi/:id'${error}`);
        });

});

router.get('/depute-profil-votes/:id', function(req, res) {
    let deputeId = parseInt(req.params.id);
    let query =[ { "memberships.riding.id": deputeId }];

    CRUDService.findOne('deputes',query[0])
        .then((element)=> {
            "use strict";
            if (!element){
                let errMsg = "" +
                    "Le serveur a traité avec succès la requête" +
                    " mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg})
            }
            res.status(200);
            res.render('pages/depute-profil-votes', {depute: element});


        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /depute-profil-votes/:id ${error}`);
        });
});


// Une requête pour obtenir au format JSON les données sur les votes d'un député
router.get('/depute-profil-votes-json/:id', function(req, res) {
    let deputeId = parseInt(req.params.id);
    let requeteDepute =[ { "memberships.riding.id": deputeId }];

    // 1ère requête, on récupère la fiche du député. On a besoin de son "lien député"
    // pour identifier ses votes.
    CRUDService.findOne('deputes', requeteDepute[0])
        .then((deputeTrouve)=> {
            "use strict";
            if (!deputeTrouve) {
                let errMsg =
                    "Le serveur a traité avec succès la requête, " +
                    "mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg });
            }

            
            // 2ième requête pour récupérer les votes du député
            let lienDepute = deputeTrouve.memberships[0].url;
            let requeteVotes = { "vote_politicians.politician_membership_url" : lienDepute };

            CRUDService.find('projetsloi', requeteVotes, 0, { home_chamber: 1, name: 1, vote_politicians: { $elemMatch: { politician_membership_url: lienDepute }}})
                .then((votes)=> {
                    "use strict";

                    var lois = {data:[]};
                    for (var i in votes) {
                        lois.data.push(
                            [
                                votes[i].name.fr,
                                votes[i].vote_politicians[0].ballot
                            ]
                        )
                    }

                    res.status(200);
                    res.render('pages/donnees-brutes', {donnees : JSON.stringify(lois)});
                });
        }) // Fin .then((deputeTrouve)
        .catch((error)=> {
            "use strict";
            console.log(`Erreur route /depute-profil-votes-json/:id : ${error}`);
        });
});

router.get('/deputes-json', function(req, res) {
    
    let requete =[{}];

    // 1ère requête, on récupère la fiche du député. On a besoin de son "lien député"
    // pour identifier ses votes.
    CRUDService.find('deputes', requete[0], 0, {name: 1, "memberships.riding.id": 1})
        .then((deputesTrouves)=> {
            "use strict";
            if (!deputesTrouves) {
                let errMsg =
                    "Le serveur a traité avec succès la requête, " +
                    "mais aucun contenu n'a été trouvé " ;
                
               
            }
             res.status(200);
             res.render('pages/donnees-brutes', {donnees : JSON.stringify(deputesTrouves)});
            // 2ième requête pour récupérer les votes du député
            

                   
                
        }) // Fin .then((deputeTrouve)
        .catch((error)=> {
            "use strict";
            console.log(`Erreur route /depute-profil-votes-json/:id : ${error}`);
        });
});


router.get('/rechercher', function(req, res) {
    let name = req.query.name;
    apiService.getNameFromPostalCode(name)
        .then((name)=>{
            let pageSize = 12;
            let query =[ {"name": {$regex:"(?i)" + name }} ];
             CRUDService.find('deputes',query[0], pageSize)
                .then((elements)=> {
                    if (elements.length == 0){
                        console.log("ADASDSAD");
                        let errMsg = "" +
                            "Le serveur a traité avec succès la requête" +
                            " mais aucun contenu n'a été trouvé " ;
                        res.status(200).render('error', {  message: errMsg})
                    } else if (elements.length == 1) {
                      res.status(200);
                      res.render('pages/depute-profil-infos', {depute: elements[0]});
                   } else {
                      res.status(200);
                      res.render('pages/deputes', {politiciens: elements});
                   }

                })
                .catch((error)=> {
                    "use strict";
                    console.log(`From index.js /rechercher ${error}`);
                });
        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /rechercher ${error}`);
        });

});

//==================================================================
//   the routes that use the "projetsloi" collection
//     - /loi/:id
//     - /projet-loi
//==================================================================

router.get('/loi/id/:id' , function(req,res) {
    let loiId = parseInt(req.params.id);
    let query =[ {"legisinfo_id": loiId}];

    CRUDService.findOne('projetsloi',query[0])
        .then((loiTrouve)=> {
            "use strict";
            if (!loiTrouve){
                let errMsg = "" +
                    "Le serveur a traité avec succès la requête" +
                    " mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg})
            }
            res.status(200);
            res.render('pages/loi', {loi: loiTrouve});
        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /loi/:id'${error}`);
        });

});
router.get('/projet-loi', function(req, res) {
    let query = [{}];
    let pageSize = 12;
    CRUDService.find('projetsloi',query[0],pageSize)
        .then((elements)=> {
            "use strict";
            if (elements.length == 0){
                let errMsg = "" +
                    "Le serveur a traité avec succès la requête" +
                    " mais aucun contenu n'a été trouvé " ;
                res.status(200).render('error', {  message: errMsg})
            }
            res.status(200);
            res.render('pages/projet-loi', {votes: elements});
        })
        .catch((error)=> {
            "use strict";
            console.log(`From index.js /projet-loi ${error}`);
        });

});


module.exports = router;
