### INF6150 - Génie logiciel: conduite de projets informatiques
#### Projet de session – Hiver 2017 
---
##Projet
Le but de notre projet est de créer un site web qui permettrait aux citoyens canadiens 
de facilement trouver et de comprendre les actions de leurs députés via une interface 
conviviale. Cette plateforme se veut un outil informatif pour la population, 
simple à utiliser pour les débutants mais puissant dans les mains d’un expert.

##Commençant

1. Assurez-vous d'avoir installé [NodeJS](https://nodejs.org/) et [npm](https://www.npmjs.com/).
2. Naviguer a la racine du project.
3. Installer les dépendances.
    ```
        npm install
    ```
4. Lancez l'application.
    ```
        npm start
    ```

##Fonctionnalités
1. A1
    ```
        L'importation de données  est faite automatiquement chaque jour à minuit.
    ```


C'est tout!